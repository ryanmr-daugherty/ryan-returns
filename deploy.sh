

echo "removing /dist"
rm -rf dist

echo "generating"
npm run build

echo "sync'ing"
# use -anv to test syncing
rsync -av ./dist/ ryan@ifupdown.com:www/ifupdown.com/public_html/ryan-returns/
