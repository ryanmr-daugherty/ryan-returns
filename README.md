# ryan-returns

> A simple countdown website to determine when Ryan departs/departed/returns to the Dev Center.

Built with `vue` and `vue-cli`!

Visit on [via adept.work](https://adept.work/ryanreturns)!

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build
